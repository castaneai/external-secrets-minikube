MINIKUBE_PROFILE := esminikube

up:
	minikube -p $(MINIKUBE_PROFILE) start
	helmfile sync
	kubectl apply -f servicemonitor.yaml
	kubectl apply -f test-secretstore.yaml
	kubectl apply -f test-secret.yaml

down:
	minikube -p $(MINIKUBE_PROFILE) stop

delete:
	minikube -p $(MINIKUBE_PROFILE) delete

up-grafana:
	minikube -p $(MINIKUBE_PROFILE) service kube-prometheus-stack-grafana
